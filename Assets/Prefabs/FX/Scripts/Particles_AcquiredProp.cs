﻿using UnityEngine;
using System.Collections;


public class Particles_AcquiredProp : MonoBehaviour
{
    private ParticleSystem m_partSys;

    void Start()
    {
        m_partSys = GetComponent<ParticleSystem>();

        m_partSys.Play();
    }    
}
