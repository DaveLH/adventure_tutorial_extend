using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Interactable))]
public class InteractableEditor : EditorWithSubEditors<ConditionCollectionEditor, ConditionCollection>
{
    private Interactable m_interactable;   // Target object we're editing

    private SerializedProperty m_sprpInteractionLocationProperty;
    private SerializedProperty m_sprpCollectionsProperty;
    private SerializedProperty m_sprpDefaultReactionCollectionProperty;

    private const float m_fCollectionButtonWidth = 125f;

    private const string m_sInteractablePropInteractionLocationName = "interactionLocation";
    private const string m_sInteractablePropConditionCollectionsName = "conditionCollections";
    private const string m_sInteractablePropDefaultReactionCollectionName = "defaultReactionCollection";


    private void OnEnable ()
    {
        m_interactable = (Interactable)target;   // Get target object ref

        m_sprpCollectionsProperty               = serializedObject.FindProperty(m_sInteractablePropConditionCollectionsName);
        m_sprpInteractionLocationProperty       = serializedObject.FindProperty(m_sInteractablePropInteractionLocationName);
        m_sprpDefaultReactionCollectionProperty = serializedObject.FindProperty(m_sInteractablePropDefaultReactionCollectionName);
        
        CheckAndCreateSubEditors(m_interactable.conditionCollections);
    }


    private void OnDisable ()
    {
        CleanupEditors ();
    }


    protected override void SubEditorSetup(ConditionCollectionEditor editor)
    {
        editor.collectionsProperty = m_sprpCollectionsProperty;
    }


    /// <summary>
    /// Format and display inpector editor controls
    /// </summary>
    /// 
    public override void OnInspectorGUI ()
    {
        serializedObject.Update ();
        
        CheckAndCreateSubEditors(m_interactable.conditionCollections);
        
        EditorGUILayout.PropertyField (m_sprpInteractionLocationProperty);

        for (int i = 0; i < subEditors.Length; i++)
        {
            subEditors[i].OnInspectorGUI ();

            EditorGUILayout.Space ();
        }

        EditorGUILayout.BeginHorizontal();

        GUILayout.FlexibleSpace ();

        if (GUILayout.Button("Add Collection", GUILayout.Width(m_fCollectionButtonWidth)))
        {
            ConditionCollection newCollection = ConditionCollectionEditor.CreateConditionCollection ();

            m_sprpCollectionsProperty.AddToObjectArray (newCollection);
        }

        EditorGUILayout.EndHorizontal ();

        EditorGUILayout.Space ();

        EditorGUILayout.PropertyField (m_sprpDefaultReactionCollectionProperty);

        serializedObject.ApplyModifiedProperties ();
    }
}
