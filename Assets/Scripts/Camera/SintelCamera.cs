﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Extend FreeLookCam functionality
/// </summary>
/// 
public class SintelCamera : UnityDLHAssets.FreeLookCam
{
    protected Camera m_camera;   // The actual camera

    protected override void Awake()
    {
        base.Awake();

        m_camera = GetComponentInChildren<Camera>();

        DontDestroyOnLoad(gameObject);
    }


    protected override void Update()
    {
        base.Update();

        // Scroll wheel zooms
        //
        if(Input.GetAxis("Mouse ScrollWheel") > 0.0f)
        {
            m_camera.fieldOfView--;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0.0f)
        {
            m_camera.fieldOfView++;
        }
    }
}
