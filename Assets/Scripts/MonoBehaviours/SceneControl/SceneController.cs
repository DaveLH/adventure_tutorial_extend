﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneController : MonoBehaviour
{
    // Delegates
    //
    public event Action BeforeSceneUnload;
    public event Action AfterSceneLoad;

    public CanvasGroup faderCanvasGroup;

    public CameraControl cameraCtrl;

    public float fadeDuration = 1f;

    public string startingSceneName = "SecurityRoom";

    public string initialStartingPositionName = "DoorToMarket";

    public SaveData playerSaveData;

    private bool m_bIsFading;


    private IEnumerator Start ()
    {
        faderCanvasGroup.alpha = 1f;

        playerSaveData.Save (UnityDLHAssets.PlayerPACC.startingPositionKey, initialStartingPositionName);

        // Load first scene
        //
        yield return StartCoroutine(LoadSceneAndSetActive(startingSceneName));

        // Fade in
        //
        StartCoroutine(Fade(0.0f));
    }


    public void FadeAndLoadScene (SceneReaction sceneReaction)
    {
        if (!m_bIsFading)
        {
            StartCoroutine(FadeAndSwitchScenes(sceneReaction.sceneName));
        }
    }


    private IEnumerator FadeAndSwitchScenes(string sSceneName)
    {
        yield return StartCoroutine(Fade(1.0f));

        if (BeforeSceneUnload != null)
        {
            BeforeSceneUnload();
        }

        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);

        yield return StartCoroutine(LoadSceneAndSetActive(sSceneName));

        if (AfterSceneLoad != null)
        {
            AfterSceneLoad();
        }

        yield return StartCoroutine(Fade(0.0f));
    }


    private IEnumerator LoadSceneAndSetActive(string sSceneName)
    {
        yield return SceneManager.LoadSceneAsync(sSceneName, LoadSceneMode.Additive);

        Scene newlyLoadedScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);

        SceneManager.SetActiveScene(newlyLoadedScene);
    }


    private IEnumerator Fade(float fFinalAlpha)
    {
        m_bIsFading = true;

        faderCanvasGroup.blocksRaycasts = true;

        float fFadeSpeed = Mathf.Abs(faderCanvasGroup.alpha - fFinalAlpha) / fadeDuration;

        while(!Mathf.Approximately(faderCanvasGroup.alpha, fFinalAlpha))
        {
            faderCanvasGroup.alpha = Mathf.MoveTowards(faderCanvasGroup.alpha, fFinalAlpha, fFadeSpeed * Time.deltaTime);

            yield return null;
        }

        faderCanvasGroup.blocksRaycasts = m_bIsFading = false;
    }
}
