﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component handling the starting point for character in current scene
/// </summary>
/// <remarks>
/// OWNER: An empty object designating the transforms the character will appear at in the scene when it starts
///    (e.g. "DoorToMarketStartingPosition")
/// </remarks>
/// 
public class StartingPosition : MonoBehaviour
{
    public string startingPointName;   // Name of starting point (Needed to lookup in database)

    // Static Database of starting positions for every active scene in game
    //
    private static List<StartingPosition> allStartingPositions =  new List<StartingPosition> ();


    // Add this starting pos. to database
    //
    private void OnEnable ()
    {
        allStartingPositions.Add (this);
    }


    // Remove this starting pos. from database
    //
    private void OnDisable ()
    {
        allStartingPositions.Remove (this);
    }


    /// <summary>
    /// Lookup a starting point in database
    /// </summary>
    /// <param name="pointName">Name of starting point object</param>
    /// <returns>Get the transform designating the player's starting point, or null if not in database</returns>
    /// <remarks>
    /// CALLED BY: PlayerPACC->SetStartingPosition()
    /// </remarks>
    /// 
    public static Transform FindStartingPosition (string pointName)
    {
        for (int i = 0; i < allStartingPositions.Count; i++)
        {
            if (allStartingPositions[i].startingPointName == pointName)
                return allStartingPositions[i].transform;
        }

        return null;
    }
}
