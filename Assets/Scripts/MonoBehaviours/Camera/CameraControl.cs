﻿using System.Collections;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public bool moveCamera = true;

    public float smoothing = 7f;

    public Vector3 offset = new Vector3 (0f, 1.5f, 0f);

    Transform m_trPlayerPosition;

    public static GameObject player;


    private IEnumerator Start ()
    {
        if(!moveCamera)
            yield break;

        player = GameObject.FindGameObjectWithTag("Player");

        yield return null;

        transform.rotation = Quaternion.LookRotation(m_trPlayerPosition.position - transform.position + offset);
    }

    
    private void Update()
    {
        if (player) m_trPlayerPosition = player.transform;
    }
    

    private void LateUpdate ()
    {
        if (!moveCamera)
            return;

        Quaternion newRotation = Quaternion.LookRotation (m_trPlayerPosition.position - transform.position + offset);

        transform.rotation = Quaternion.Slerp (transform.rotation, newRotation, Time.deltaTime * smoothing);
    }
}
