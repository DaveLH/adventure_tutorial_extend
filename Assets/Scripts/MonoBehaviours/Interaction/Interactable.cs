﻿using UnityEngine;

/// <summary>
/// Component to allow a prop or NPC to interact with player
/// </summary>
/// <remarks>
/// OWNER: Root of a prop or NPC object
/// </remarks>
/// 
public class Interactable : MonoBehaviour
{
    // Where the player will stand to interact with object
    //
    public Transform interactionLocation;

    // Collection of conditions that govern the reactions to interacting with player
    //
    public ConditionCollection[] conditionCollections = new ConditionCollection[0];

    // Default reaction if other conditions aren't met
    //
    public ReactionCollection defaultReactionCollection;

    /// <summary>
    /// Evaluate current conditions and react accordingly
    /// </summary>
    /// 
    public void Interact ()
    {
        for (int i = 0; i < conditionCollections.Length; i++)
        {
            if (conditionCollections[i].CheckAndReact ())
                return;
        }

        // If conditions not met, run deault reaction
        //
        defaultReactionCollection.React ();
    }
}
