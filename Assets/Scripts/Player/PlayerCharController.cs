﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Basic movement and animation control for the player character 
/// </summary>
/// <remarks>
/// OWNER: Root of Player asset hierarchy
/// SUBCLASSES:  
///   PlayerFPC  -- First-Person (Potentially)
///   PlayerTPC  -- Third-Person (TODO)
///   PlayerPACC -- "Point-And-Click" Control
/// </remarks>
/// 
namespace UnityDLHAssets
{
    public abstract class PlayerCharController : MonoBehaviour
    {
        protected Animator m_anim;


        protected virtual void Start()
        {
            m_anim = GetComponent<Animator>();
            
            DontDestroyOnLoad(gameObject);
        }

        /// <summary>
        /// Get direction and speed to move player from device independent input
        /// </summary>
        /// <returns>Vector representing direction and speed of desired movement</returns>
        /// 
        public abstract Vector3 GetLocomotionInput();

        /// <summary>
        /// Move the character based on move vector
        /// </summary>
        /// <param name="v3MoveVector"></param>
        /// 
        public abstract void Move(Vector3 v3MoveVector);
    } 
}
