﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace UnityDLHAssets
{
    /// <summary>
    /// Class to implement TPC player character's movement driven by pointing
    ///   and clicking on destination or object to interact with
    /// </summary>
    /// <remarks>
    /// OWNER: Root of player character prefab
    /// 
    /// NOTE: As written, this class assumes a copy of the player character pre-exists 
    ///   in every scene asset (no need to load prefab at scene startup or do DontDestoryOnLoad())
    /// </remarks>
    /// 
    public class PlayerPACC : MonoBehaviour
    {
        public float inputHoldDelay     = 0.5f;
        public float turnSpeedThreshold = 0.5f;
        public float speedDampTime      = 0.1f;
        public float slowingSpeed       = 0.175f;
        public float turnSmoothing      = 15.0f;

        public SaveData playerSaveData;

        protected Animator     m_anim;
        protected NavMeshAgent m_nmAgent;

        protected WaitForSeconds m_waitInputHold;

        protected Vector3 m_v3DestinationPos;

        protected Interactable m_currentInteractObj;

        protected bool m_bHandleInput = true;

        const float StopDistRatio     = 0.1f;
        const float NavMeshSampleDist = 4.0f;

        public const string startingPositionKey = "starting position";

        #region Hash codes for Animator Params

        readonly int hashAnimSpeed       = Animator.StringToHash("Speed");
        readonly int hashLocomotionState = Animator.StringToHash("isLocomotionState");
        readonly int hashLocomotionTag   = Animator.StringToHash("Locomotion");

        #endregion

        #region Animator-related Properties

        public bool isLocomotionState    // Is player in a locomotion state?
        {
            get
            {
                return (m_anim.GetCurrentAnimatorStateInfo(0).tagHash == hashLocomotionTag);
            }
        }

        #endregion


        /// <summary>
        /// At the start of a scene, place character at designated starting point
        /// </summary>
        /// 
        protected void SetStartingPosition()
        {
            string startingPositionName = "";

            // Get desired starting position name from runtime database
            //  -- If player has been here before, this will (normally) restore the player
            //      to the point at which they previously left.
            //
            playerSaveData.Load(startingPositionKey, ref startingPositionName);

            // Look for starting position of that name in list of starting
            //   positions for current scene
            //
            Transform startingPosition = StartingPosition.FindStartingPosition(startingPositionName);

            // If found, place chatacter at that transform
            //
            transform.position = startingPosition.position;
            transform.rotation = startingPosition.rotation;
        }


        protected void Start()
        {
            Debug.Log("Initializing player ... ");

            m_anim    = GetComponent<Animator>();
            m_nmAgent = GetComponent<NavMeshAgent>();

            Debug.Log("(Anim, NavAgent) = " + m_anim.ToString() + " ; " + m_nmAgent.ToString());

            m_nmAgent.updateRotation = false;   // Rotation will be done manually in script

            m_waitInputHold = new WaitForSeconds(inputHoldDelay);

            SetStartingPosition();   // Place the player at starting posiiton in scene

            m_v3DestinationPos = transform.position;

            // DontDestroyOnLoad(gameObject);
        }


        protected void Update()
        {
            if(isLocomotionState) UpdateNavMeshAgent();
        }


        protected void UpdateNavMeshAgent()
        {
            if (m_nmAgent.pathPending) return;  // Wait for path calculation to finish
            //
            else
            {
                float fSpeed = m_nmAgent.desiredVelocity.magnitude;

                if (m_nmAgent.remainingDistance <= m_nmAgent.stoppingDistance * StopDistRatio)
                {
                    Stopping(out fSpeed);   // CLose enough to destination to stop
                }
                else if (m_nmAgent.remainingDistance <= m_nmAgent.stoppingDistance)
                {
                    Slowing(out fSpeed, m_nmAgent.remainingDistance);   // CLose enough to destination to slow down toward a stop
                }
                else if (fSpeed > turnSpeedThreshold)
                {
                    TurnWhileMoving();   // Handle making a turn
                }

                m_anim.SetFloat(hashAnimSpeed, fSpeed, speedDampTime, Time.deltaTime);   // Set speed param in state machine
            }
        }


        protected void OnAnimatorMove()
        {
            m_nmAgent.velocity = m_anim.deltaPosition / Time.deltaTime;


        }


        protected void Stopping(out float speed)
        {
            m_nmAgent.Stop();

            transform.position = m_v3DestinationPos;

            speed = 0.0f; 

            if (m_currentInteractObj)  // If an interactable entity is in range
            {
                // Align player with interaction object at interaction point
                //
                transform.rotation = m_currentInteractObj.interactionLocation.rotation;

                // Player interacts with NPC or object
                //
                m_currentInteractObj.Interact();

                // Ensure this block only runs once
                //
                m_currentInteractObj = null;

                // Start waiting for interaction to run
                //
                StartCoroutine(WaitForInteraction());
            }
        }


        protected void Slowing(out float speed, float distToDest)
        {
            m_nmAgent.Stop();

            transform.position = Vector3.MoveTowards(transform.position, m_v3DestinationPos, slowingSpeed * Time.deltaTime);

            float proportDistance = 1.0f - distToDest / m_nmAgent.stoppingDistance;

            speed = Mathf.Lerp(slowingSpeed, 0.0f, proportDistance);  // Decerate based on closenes to destination point

            // Determine rotation
            //
            Quaternion qTargetRot = m_currentInteractObj ? m_currentInteractObj.interactionLocation.rotation : transform.rotation;
            //
            transform.rotation = Quaternion.Lerp(transform.rotation, qTargetRot, proportDistance);
        }


        protected void TurnWhileMoving()
        {
            Quaternion qTargetRot = Quaternion.LookRotation(m_nmAgent.desiredVelocity);

            transform.rotation = Quaternion.Lerp(transform.rotation, qTargetRot, turnSmoothing * Time.deltaTime);
        }


        /// <summary>
        /// When user clicks on a point on the NavMesh, make that
        ///   the destination point and trigger walking (with the NavMeshAgent) 
        ///   to that point
        /// </summary>
        /// <param name="data">Event data containing info on the point that was clicked</param>
        /// <remarks>
        /// INVOKED BY: Pointer Click Event Trigger on Root object of environment containing NavMesh
        /// </remarks>
        /// 
        public void OnGroundClick(BaseEventData data)
        {
            if (m_bHandleInput)
            {
                NavMeshHit hit;

                PointerEventData pntData = (PointerEventData)data;

                m_currentInteractObj = null;

                if (NavMesh.SamplePosition(pntData.pointerCurrentRaycast.worldPosition, out hit, NavMeshSampleDist, NavMesh.AllAreas))
                {
                    // Set destination point at click point
                    //
                    m_v3DestinationPos = hit.position;
                }
                else m_v3DestinationPos = pntData.pointerCurrentRaycast.worldPosition;

                SetNavDestination(); 
            }
        }


        /// <summary>
        /// When user clicks on an interactable object, trigger walking to object
        ///   and then doing interaction
        /// </summary>
        /// <param name="interactObj">The component "Interactable" of an
        ///   interactable object</param>
        /// <remarks>
        /// INVOKED BY: Pointer Click Event Trigger on Interactable object
        /// </remarks>
        /// 
        public void OnInteractableClick(Interactable interactObj)
        {
            if (m_bHandleInput)
            {
                m_currentInteractObj = interactObj;

                m_v3DestinationPos = m_currentInteractObj.interactionLocation.position;

                SetNavDestination();
            }
        }


        /// <summary>
        /// Set new destination for NavMeshAgent to current destination and start moving towards it
        /// </summary>
        /// <remarks>
        /// CALLED BY: OnGroundClick(), OnInteractableClick()
        /// </remarks>
        /// 
        protected void SetNavDestination()
        {
            // Start agent moving towards destination
            //
            m_nmAgent.SetDestination(m_v3DestinationPos);
            m_nmAgent.Resume();
        }


        /// <summary>
        /// If player is interacting with NPC or prop, wait for interaction to complete
        ///   before allowing further user input
        /// </summary>
        /// <returns>(Coroutine)</returns>
        /// <remarks>
        /// INVOKED BY: Stopping()
        /// </remarks>
        /// 
        protected IEnumerator WaitForInteraction()
        {
            m_bHandleInput = false;   // Disable user input

            // Debug.Log("WaitForInteraction(): m_bHandleInput = false");

            yield return m_waitInputHold;   // Wait for a few seconds

            // Wait until interaction anim has finished, and
            //  player has returned to a non-interaction state
            //
            while (!isLocomotionState)  // This test works because...
            {
                yield return null;   // ... state machine passes thru "Walking" before returning to Idle
            }

            m_bHandleInput = true;   // Re-enable user input

            // Debug.Log("WaitForInteraction(): m_bHandleInput = true");
        }
    } 
}
