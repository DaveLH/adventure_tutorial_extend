﻿using UnityEngine;
using System.Collections;

namespace DLH_UnityLib
{
    /// <summary>
    /// Make a character blink, if it has appropriate shape keys
    /// </summary>
    /// 
    public class State_NoBlink : StateMachineBehaviour
    {
        public float MinWaitTime = 4f, MaxWaitTime = 6f;   // Control mean interval between blinks

        private float m_fWaitTime;    // Time to wait in non-blinking state
        private float m_fWaitClock;   // How long we've been in this state

        // OnStateEnter -- Set up waiting for next blink
        //
        /// <summary>
        /// Wait for interval, then go to "blinking" state (run "blink" anim)
        /// </summary>
        /// 
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // Verify full weight
            //
            animator.SetLayerWeight(layerIndex, 1.0f);

            // Set time to wait for next blink & reset timer
            //
            m_fWaitTime = Random.Range(MinWaitTime, MaxWaitTime);
            m_fWaitClock = 0f;
        }

        /// <summary>
        /// Tick down wait time.  When time runs out, trigger a blink (i.e. play "Blink" anim once)
        /// </summary>
        /// <param name="animator"></param>
        /// <param name="stateInfo"></param>
        /// <param name="layerIndex"></param>
        /// 
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // Update the wait timer
            //
            m_fWaitClock += Time.deltaTime;

            if (m_fWaitClock >= m_fWaitTime)
            {
                // Time's up! -- Go to "blinking" state
                //
                animator.SetTrigger("DoBlink");
            }
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}
    }
}
