﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Basic movement and animation control for the player character 
/// </summary>
/// <remarks>
/// OWNER: Root of Player asset hierarchy
/// SUBCLASSES:  
///   PlayerFPC  -- First-Person (Potentially)
///   PlayerTPC  -- Third-Person (TODO)
///   PlayerPACC -- "Point-And-Click" Control
/// </remarks>
/// 
namespace UnityDLHAssets
{
    public abstract class PlayerTPC : PlayerCharController
    {
        /*
        Animator anim;

        CharacterController charCtrl;
        PlayerAnimator animPlayer;


        void Start()
        {
            anim = GetComponent<Animator>();
            animPlayer = GetComponent<PlayerAnimator>();
            charCtrl = GetComponent<CharacterController>();
            playerJump = GetComponent<PlayerJumpClimb>();

            DontDestroyOnLoad(gameObject);
        }


        void Update()
        {
            // Default (non-combat) movement
            //
            if (!GameProgression.CombatMode)
            {
                Vector3 v3MoveVector = GetMoveVector();

                float translation = v3MoveVector.z;
                float rotation = v3MoveVector.x;

                // Move player in world space
                //
                animPlayer.WorldMove(translation, rotation);

                // Make player jump
                //
                if (playerJump)   // If PlayerJumpClimb component is present 
                {
                    playerJump.DoJumping();
                }
            }
        }




        /// <summary>
        /// Get the current Move Vector from player input
        /// </summary>
        /// 
        public Vector3 GetMoveVector()
        {
            return GameProgression.GetLocomotionInput();
        }



        void ApplyGravity()
        {
            // if(!charCtrl.isGrounded) 
        }
        */
    } 
}
