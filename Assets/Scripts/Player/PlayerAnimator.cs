﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Base Class for interfacing with our character's animation controller -- 
///   Processes PlayerController output and sets the correct animation state.
/// </summary>
/// 
namespace UnityDLHAssets
{
    public class PlayerAnimator : MonoBehaviour
    {
        // Enums for different animation states
        //
        public enum StateID { Idle, Walking, Running, Jumping };   // TODO: Add more

        Animator m_animCtlr;   // Animation controller;

        PlayerCharController m_playerCtrl;   // Reference to PlayerController

        float m_fCurrentSpeed;   // Player's current locomotion speed


        // Ref to anim controller
        //
        public Animator AnimCtrl
        {
            get { return m_animCtlr; }
        }


        void UpdateTranslation()
        {
            // Translate(m_playerCtrl.Velocity, m_playerCtrl.Direction);
        }

        /*
        /// <summary>
        /// Set Mecanim state from State IDs
        /// </summary>
        /// <param name="state">State ID</param>
        /// <param name="fSpeed">Speed to move player (Ignored if State is Idle)</param>
        /// 
        public virtual void SetAnimParams(StateID state, float fSpeed)
        {
            switch(state)
            {
                case StateID.Idle:
                    {
                        SetState_Idle(true);  break;
                    }
                case StateID.Walking:
                    {
                        SetState_Locomotion(false, fSpeed); break;
                    }
                case StateID.Running:
                    {
                        SetState_Locomotion(true, fSpeed); break;
                    }
            }
        }
        */


        void SetState_Idle()
        {
            m_fCurrentSpeed = 0.0f;

            SetState_Locomotion(false);
        }

        void SetState_Moving()
        {
            SetState_Locomotion(true);
        }

        /// <summary>
        /// Determine locomotion animation based on player's movement speed
        /// </summary>
        /// <param name="fSpeed">Current speed of root motion</param>
        /// 
        void SetState_Locomotion(bool bMoving)
        {
            m_animCtlr.SetBool("isIdle", !bMoving);
            m_animCtlr.SetBool("isWalking", bMoving);
            m_animCtlr.SetFloat("Speed", m_fCurrentSpeed);
        }


        /// <summary>
        /// Trigger animation for jumping
        /// </summary>
        /// 
        void SetState_Jump()
        {
            m_animCtlr.SetTrigger("Jump");
        }


        /// <summary>
        /// Guide character's overall velocity in world space over one frame
        /// </summary>
        /// <param name="pos">New position</param>
        /// <param name="rot">New rotation</param>
        /// 
        public void WorldMove(float fFwd, float fRot)
        {
            fFwd *= Time.deltaTime;
            fRot *= Time.deltaTime;

            m_fCurrentSpeed += fFwd;   // Accelerate character while there's forward input

            transform.Rotate(Vector3.up, fRot);  // Make character turn with sideways input

            // Set animation state
            //
            if (fFwd > 0.0f)   // Walking/Running
            {
                SetState_Moving();
            }
            else   // Idle
            {
                SetState_Idle();
            }
            // TODO: Jumping, Strafing, &c.

        }


        /// <summary>
        /// Jump animation
        /// </summary>
        /// 
        public void Jump()
        {
            SetState_Jump();
        }


        void Start()
        {
            m_animCtlr = GetComponent<Animator>();
            m_playerCtrl = GetComponent<PlayerCharController>();
        }


        void Update()
        {

        }
    } 
}

