﻿using UnityEngine;

/// <summary>
/// Base class for all reactions triggered by a set of conditions
/// </summary>
/// 
public abstract class Reaction : ScriptableObject
{
    public void Init ()
    {
        SpecificInit ();
    }


    protected virtual void SpecificInit()
    {}


    public void React (MonoBehaviour monoBehaviour)
    {
        ImmediateReaction ();
    }

    // Call when a reaction should be immediate
    //
    protected abstract void ImmediateReaction ();
}
