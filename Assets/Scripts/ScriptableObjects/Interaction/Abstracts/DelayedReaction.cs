﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for all reactions that occur after a delay
/// </summary>
/// 
public abstract class DelayedReaction : Reaction
{
    public float delay;


    protected WaitForSeconds wait;


    public new void Init ()
    {
        wait = new WaitForSeconds (delay);

        SpecificInit ();
    }


    // MonoBehaviour arg is used to run coroutine to delay the reaction
    //
    public new void React (MonoBehaviour monoBehaviour)
    {
        monoBehaviour.StartCoroutine (ReactCoroutine ());
    }


    protected IEnumerator ReactCoroutine ()
    {
        yield return wait;   // Wait...

        ImmediateReaction ();   // ... Then react
    }
}
