﻿using UnityEngine;

public class ConditionCollection : ScriptableObject
{
    public string description;

    // Required conditions to satisfy for a set of reactions to take place
    //
    public Condition[] requiredConditions = new Condition[0];

    // The reactions that will happen if conditions are all met
    //
    public ReactionCollection reactionCollection;


    /// <summary>
    /// Evaluate current conditions and react accordingly
    /// </summary>
    /// 
    public bool CheckAndReact()
    {
        // Scan all required conditions -- All must be satisfied, or function returns and
        //   Default reaction is executed
        //
        for (int i = 0; i < requiredConditions.Length; i++)
        {
            if (!AllConditions.CheckCondition (requiredConditions[i]))
                return false;
        }

        // If all conditions met, execute corresponding reaction
        //
        if(reactionCollection)
            reactionCollection.React();

        return true;
    }
}